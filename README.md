# Containerization and Orchestration Technologies - Final Project

## Lien vers l'application

L'application est disponible à l'adresse suivante : 10.195.68.241.

## Lancement du projet

**Online Boutique** est une application de démonstration de microservices en nuage.  Il s'agit d'une application de commerce électronique basée sur le web où les utilisateurs peuvent parcourir les articles, les ajouter au panier et les acheter.

Pour déployer l'application sur un cluster kubernetes, suivre les instructions suivantes :

* Connexion au serveur via ssh : `ssh username@hostname`
* Installer microk8s sur le cluster :

```bash
sudo snap install microk8s --classic
# Ajout du user dans le groupe microk8s
sudo usermod -a -G microk8s
# créer le dossier .kube
mkdir -p $HOME/.kube
# Changer l'owner du dossier
sudo chown -R ubuntu ~/.kube
newgrp microk8s
# verification de l'installation 
microk8s version
# alias helm
alias helm=microk8s.helm
# alias kubectl
alias kubectl=microk8s.kubectl

```

* cloner le repositoire git : `git clone git@gitlab.unige.ch:Kerfalla.Cisse/kerfk-8-sproject.git`
* Permettre l'utilisation de l'ingress : `microk8s enable ingress`
* Permettre l'utilisation de l'autoscaling : `microk8s enable metrics-server`
* Appliquer la storage class : `kubectl apply -f kubernetes/storage-class.yaml`
* Déployer l'application sur le cluster (toutes les images sont publiques) : `helm install helm-chart helm-chart`

## Architecture

[![Architecture of microservices](/docs/img/architecture-diagram.png)](/docs/img/architecture-diagram.png)

Find **Protocol Buffers Descriptions** at the [`./protos` directory](/protos).

| Service                                             | Language | Description                                                                                                                       |
| --------------------------------------------------- | -------- | --------------------------------------------------------------------------------------------------------------------------------- |
| [frontend](/src/frontend)                           | Go       | Exposes an HTTP server to serve the website. Does not require signup/login and generates session IDs for all users automatically. |
| [cartservice](/src/cartservice)                     | C#       | Stores the items in the user's shopping cart in Redis and retrieves it.                                                           |
| [productcatalogservice](/src/productcatalogservice) | Go       | Provides the list of products from a JSON file and ability to search products and get individual products.                        |
| [currencyservice](/src/currencyservice)             | Node.js  | Converts one money amount to another currency. Uses real values fetched from European Central Bank. It's the highest QPS service. |
| [paymentservice](/src/paymentservice)               | Node.js  | Charges the given credit card info (mock) with the given amount and returns a transaction ID.                                     |
| [shippingservice](/src/shippingservice)             | Go       | Gives shipping cost estimates based on the shopping cart. Ships items to the given address (mock)                                 |
| [emailservice](/src/emailservice)                   | Python   | Sends users an order confirmation email (mock).                                                                                   |
| [checkoutservice](/src/checkoutservice)             | Go       | Retrieves user cart, prepares order and orchestrates the payment, shipping and the email notification.                            |
| [recommendationservice](/src/recommendationservice) | Python   | Recommends other products based on what's given in the cart.                                                                      |
| [adservice](/src/adservice)                         | Java     | Provides text ads based on given context words.                                                                                   |
